import pytest 

from os import pardir, path, listdir
from os.path import dirname, abspath

src = dirname(dirname(abspath(__file__))) + "/target/src"


def test_files_structure_1():
    assert path.exists(f"{src}"), "Assingment must be inside src folder"


@pytest.mark.parametrize("fnmae", ["Triangle.py", "Figure.py", "Circle.py", "Square.py", "Rectangle.py"])
def test_all_class_files(fnmae):
    files = listdir(src)
    assert fnmae in files, f"src folder must have {fnmae} inside" 


@pytest.mark.parametrize("atrname", ["add_area", "perimeter", "area"])
def test_classes_figure_class(atrname):
    from target.src.Figure import Figure
    assert hasattr(Figure,atrname), "class Figure must have '{atrname}' as available attribute or method"
